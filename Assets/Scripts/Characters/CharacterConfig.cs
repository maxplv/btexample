﻿using BTExample.Configs;
using UnityEngine;

namespace BTExample.Characters
{
    public class CharacterConfig : MonoBehaviour
    {
        [SerializeField] private UnitConfig _unitConfig;

        public UnitConfig UnitConfig => _unitConfig;
    }
}