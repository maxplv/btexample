﻿using UnityEngine;

namespace BTExample.Characters
{
    [RequireComponent(typeof(CharacterConfig))]
    public class MeleeAttackController : AttackController
    {
        private CharacterConfig _config;
        private int _damage;
        private Health _target;
        private float _lastHitTime;

        private void Start()
        {
            _config = GetComponent<CharacterConfig>();
            _damage = _config.UnitConfig.BasicDamage;
        }

        public override void Attack(Health target)
        {
            if (target != null && target.HP > 0)
            {
                if (Time.time >= _config.UnitConfig.HitDelay + _lastHitTime)
                {
                    _lastHitTime = Time.time;
                    target.Damage(_damage);
                }
            }
        }
    }
}
