using BTExample.Shells;
using UnityEngine;

namespace BTExample.Characters
{
    [RequireComponent(typeof(CharacterConfig))]
    public class DistantAttackController : AttackController
    {
        [SerializeField] private Shell _shell;
        
        private CharacterConfig _config;
        private float _lastHitTime;

        private void Start()
        {
            _config = GetComponent<CharacterConfig>();
        }

        public override void Attack(Health target)
        {
            if (target != null && target.HP > 0)
            {
                if (Time.time >= _config.UnitConfig.HitDelay + _lastHitTime)
                {
                    _lastHitTime = Time.time;
                    var bullet = ObjectPool.Spawn(_shell, transform.position);
                    bullet.Launch(_config.UnitConfig.BasicDamage, target.transform.position);
                }
            }
        }
    }
}
