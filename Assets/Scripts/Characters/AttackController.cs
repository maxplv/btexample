using UnityEngine;

namespace BTExample.Characters
{
    public abstract class AttackController : MonoBehaviour
    {
        public abstract void Attack(Health health);
    }
}
