﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace BTExample.Characters
{
    public class HealthSensorTrigger : MonoBehaviour
    {
        public event Action<Health> TargetIsChanged;
        
        [SerializeField] private string _enemyLayer;
        
        private List<Health> _targets;
        private int _enemyLayerId;

        public bool HasTarget => _targets.Count > 0;

        private void Start()
        {
            _targets = new List<Health>();
            _enemyLayerId = LayerMask.NameToLayer(_enemyLayer);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == _enemyLayerId)
            {
                var target = other.GetComponent<Health>();

                if (target != null)
                {
                    _targets.Add(target);
                    target.UnitKilled += OnUnitKilled;

                    if (_targets.Count == 1)
                    {
                        ChooseTarget();
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == _enemyLayerId)
            {
                var target = other.GetComponent<Health>();
                
                if (target != null)
                {
                    KickFromTargetList(target);
                }
            }
        }
        
        private void OnUnitKilled(Health target)
        {
            KickFromTargetList(target);
        }

        private void KickFromTargetList(Health target)
        {
            int index = _targets.IndexOf(target);
            
            if (index >= 0)
            {
                _targets[index].UnitKilled -= OnUnitKilled;
                _targets.RemoveAt(index);
                
                if (index == 0)
                {
                    Debug.Log("Lose target " + target.name);
                    TargetIsChanged?.Invoke(null);
                    
                    if (_targets.Count > 0)
                    {
                        ChooseTarget();
                    }
                }
            }
        }

        private void ChooseTarget()
        {
            Debug.Log("Take target " + _targets[0].name);
            TargetIsChanged?.Invoke(_targets[0]);
        }
    }
}
