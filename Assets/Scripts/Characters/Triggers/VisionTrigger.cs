using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class VisionTrigger : MonoBehaviour
    {
        [SerializeField] private string _enemyLayer;
        [SerializeField] private BTCondition _condition;
        
        private int _enemyLayerId;
        private Transform _target;

        public Transform Target => _target;

        private void Awake()
        {
            _enemyLayerId = LayerMask.NameToLayer(_enemyLayer);
            _condition.InjectCondition(() => false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == _enemyLayerId)
            {
                _condition.InjectCondition(() => true);
                _target = other.transform;
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == _enemyLayerId)
            {
                _condition.InjectCondition(() => false);
                _target = null;
            }
        }
    }
}
