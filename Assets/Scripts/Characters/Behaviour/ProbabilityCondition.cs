using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class ProbabilityCondition : MonoBehaviour
    {
        [SerializeField] private BTCondition _condition;
        [SerializeField] private CharacterConfig _config;
        
        private void Start()
        {
            _condition.InjectCondition(DoRandomize);
        }

        private bool DoRandomize()
        {
            return Random.value <= _config.UnitConfig.BonusAttackProbability;
        }
    }
}