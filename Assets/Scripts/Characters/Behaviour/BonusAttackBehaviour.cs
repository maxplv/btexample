using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class BonusAttackBehaviour : MonoBehaviour
    {
        [SerializeField] private VisionTrigger _visionTrigger;
        [SerializeField] private AttackController _attackController;
        [SerializeField] private BTAction _action;
        [SerializeField] private Transform _rootTransform;

        private Health _targetHealth;
        private bool _hasTarget;

        private void Start()
        {
            _action.InjectAction(AttackAction);
        }
        
        private BTNodeStates AttackAction()
        {
            BTNodeStates state = BTNodeStates.FAILURE;
                
            _rootTransform.LookAt(_visionTrigger.Target.position);
            
            if (!_hasTarget)
            {
                _targetHealth = _visionTrigger.Target.GetComponent<Health>();
                _hasTarget = true;
            }
            _attackController.Attack(_targetHealth);
            
            return state;
        }
    }
}