using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class Idle : MonoBehaviour
    {
        [SerializeField] private BTAction _actionNode;

        private void Awake()
        {
            _actionNode.InjectAction(() => { return BTNodeStates.RUNNING; });
        }
    }
}
