using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class AttackBehaviour : MonoBehaviour
    {
        [SerializeField] private HealthSensorTrigger _healthSensor;
        [SerializeField] private AttackController _attackController;
        [SerializeField] private BTAction _action;
        [SerializeField] private Transform _rootTransform;

        private Health _target;
        
        private void Start()
        {
            _healthSensor.TargetIsChanged += OnTargetChange;
            _action.InjectAction(AttackAction);
        }
        
        private void OnDestroy()
        {
            _healthSensor.TargetIsChanged -= OnTargetChange;
        }
        
        private void OnTargetChange(Health target)
        {
            _target = target;
        }
        
        private BTNodeStates AttackAction()
        {
            BTNodeStates state = BTNodeStates.FAILURE;
            
            if (_healthSensor.HasTarget)
            {
                state = BTNodeStates.RUNNING;
                
                _rootTransform.LookAt(_target.transform.position);
                _attackController.Attack(_target);
            }
            
            return state;
        }
    }
}