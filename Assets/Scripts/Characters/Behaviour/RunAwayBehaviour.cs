using BTExample.BT;
using UnityEngine;
using UnityEngine.AI;

namespace BTExample.Characters
{
    public class RunAwayBehaviour : MonoBehaviour
    {
        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private VisionTrigger _vision;
        [SerializeField] private CharacterConfig _config;
        [SerializeField] private BTAction _action;

        private Vector3 _direction;
        
        private void Start()
        {
            _action.InjectAction(RunAwayAction);
            _direction = new Vector3(Random.Range(-1f, 1f), 0, Random.Range(-1f, 1f)).normalized; 
        }

        private BTNodeStates RunAwayAction()
        {
            BTNodeStates state = BTNodeStates.FAILURE;

            if (_vision.Target != null)
            {
                state = BTNodeStates.RUNNING;
                _agent.velocity = _direction * _config.UnitConfig.Speed;
            }

            return state;
        }
    }
}