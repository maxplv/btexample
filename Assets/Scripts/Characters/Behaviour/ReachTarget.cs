using BTExample.BT;
using UnityEngine;
using UnityEngine.AI;

namespace BTExample.Characters
{
    public class ReachTarget : MonoBehaviour
    {
        [SerializeField] private BTAction _action;
        [SerializeField] private NavMeshAgent _agent;
        [SerializeField] private VisionTrigger _visionTrigger;
        [SerializeField] private HealthSensorTrigger _healthSensor;
        [SerializeField] private CharacterConfig _config;
        [SerializeField] private Health _health;

        private void Awake()
        {
            _action.InjectAction(ReachTargetAction);
        }

        private BTNodeStates ReachTargetAction()
        {
            BTNodeStates state = BTNodeStates.FAILURE;

            if (_health.HP > _config.UnitConfig.RunAwayHealth && !_healthSensor.HasTarget
                                                              && _visionTrigger.Target != null)
            {
                state = BTNodeStates.RUNNING; 
                _agent.velocity = (_visionTrigger.Target.position - _agent.transform.position).normalized 
                                  * _config.UnitConfig.Speed;
            }

            return state;
        }
    }
}