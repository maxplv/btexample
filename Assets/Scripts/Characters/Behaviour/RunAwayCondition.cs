using BTExample.BT;
using UnityEngine;

namespace BTExample.Characters
{
    public class RunAwayCondition : MonoBehaviour
    {
        [SerializeField] private BTCondition _condition;
        [SerializeField] private Health _health;
        [SerializeField] private CharacterConfig _characterConfig;
        
        private bool _runAway;
        
        private void Start()
        {
            _condition.InjectCondition(DoAttack);
        }

        private bool DoAttack()
        {
            return _health.HP > _characterConfig.UnitConfig.RunAwayHealth;
        }
    }
}