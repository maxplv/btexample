﻿using System;
using UnityEngine;

namespace BTExample.Characters
{
    [RequireComponent(typeof(CharacterConfig))]
    public class Health : MonoBehaviour
    {
        public event Action<Health> UnitKilled;
        
        private CharacterConfig _config;
        private int _hp;

        public int HP => _hp;

        private void Start()
        {
            _config = GetComponent<CharacterConfig>();
            _hp = _config.UnitConfig.HP;
        }

        public void Heal(int hp)
        {
            _hp = Math.Min(_config.UnitConfig.HP, _hp + hp);
        }

        public void Damage(int hp)
        {
            _hp -= hp;
            
            Debug.Log($"{name} is damaged. HP: {_hp}");

            if (_hp <= 0)
            {
                UnitKilled?.Invoke(this);
                
                //TODO: Remove this test code;
                DestroyImmediate(gameObject);
            }
        }
    }
}
