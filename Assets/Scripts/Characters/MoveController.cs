﻿using BTExample.Common;
using BTExample.Managers;
using UnityEngine;
using UnityEngine.AI;

namespace BTExample.Characters
{
    [RequireComponent(typeof(CharacterConfig))]
    [RequireComponent(typeof(NavMeshAgent))]
    public class MoveController : MonoBehaviour, IUpdatable
    {
        [SerializeField] private float _rotationSpeed;
        
        private NavMeshAgent _navAgent;
        private Joystick _joystick;
        private CharacterConfig _config;
        
        private void Start()
        {
            _navAgent = GetComponent<NavMeshAgent>();
            _navAgent.angularSpeed = _rotationSpeed;
            _joystick = ManagersFacade.Instance.InputManager.Joystick;
            ManagersFacade.Instance.UpdateManager.AddToUpdateList(this);
            _config = GetComponent<CharacterConfig>();
        }

        public void DoUpdate()
        {
            Vector3 direction = new Vector3(_joystick.Horizontal, 0f, _joystick.Vertical);
            _navAgent.velocity = direction * _config.UnitConfig.Speed;
        }
    }
}