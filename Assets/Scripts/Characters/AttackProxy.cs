using BTExample.Common;
using BTExample.Managers;
using UnityEngine;

namespace BTExample.Characters
{
    public class AttackProxy : MonoBehaviour, IUpdatable
    {
        [SerializeField] private HealthSensorTrigger _healthSensor;
        [SerializeField] private MeleeAttackController _attackController;

        private UpdateManager _updateManager;
        private Health _target;
        private bool _hasTarget;
        
        private void Start()
        {
            _updateManager = ManagersFacade.Instance.UpdateManager;
            _updateManager.AddToUpdateList(this);
            _healthSensor.TargetIsChanged += OnTargetChange;
        }
        
        private void OnDestroy()
        {
            _updateManager.RemoveFromList(this);
            _healthSensor.TargetIsChanged -= OnTargetChange;
        }
        
        public void DoUpdate()
        {
            if (_hasTarget)
            {
                _attackController.Attack(_target);
            }
        }

        private void OnTargetChange(Health target)
        {
            _hasTarget = target != null;
            _target = target;
        }
    }
}
