using UnityEngine;

namespace BTExample.Shells
{
    public class Grenade : Shell
    {
        private float _distance;
        private Vector3 _startPosition;
        private Vector3 _horizontalPosition;

        public override void Launch(int damage, Vector3 targetPosition)
        {
            base.Launch(damage, targetPosition);
            
            _distance = (targetPosition - transform.position).magnitude;
            _startPosition = transform.position;
            _horizontalPosition = transform.position;
        }

        protected override void Move()
        {
            _horizontalPosition += Config.Speed * Time.deltaTime * Direction;
            float currentDist = (_horizontalPosition - _startPosition).magnitude;
            var completeness = currentDist / _distance;

            if (completeness <= 1)
            {
                var pos = _horizontalPosition;
                pos.y = _startPosition.y + Config.Trajectory.Evaluate(completeness) * Config.MaxHeight;
                transform.position = pos;
            }
            else
            {
                DestroyObject();
            }
        }
    }
}