using UnityEngine;

namespace BTExample.Shells
{
    public class Bullet : Shell
    {
        protected override void Move()
        {
            transform.position += Config.Speed * Time.deltaTime * Direction;
        }
    }
}