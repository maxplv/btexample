using BTExample.Characters;
using BTExample.Common;
using BTExample.Configs;
using BTExample.Managers;
using UnityEngine;

namespace BTExample.Shells
{
    public abstract class Shell : MonoBehaviour, IUpdatable
    {
        [SerializeField] private ShellConfig _config;
        [SerializeField] private string _enemyLayer;

        private int _damage;
        private Vector3 _direction;
        private UpdateManager _updateManager;
        private float _launchTime;
        private int _enemyLayerId;

        protected ShellConfig Config => _config;
        protected Vector3 Direction => _direction;

        protected abstract void Move();

        private void Awake()
        {
            _updateManager = ManagersFacade.Instance.UpdateManager;
            _enemyLayerId = LayerMask.NameToLayer(_enemyLayer);
        }

        public virtual void Launch(int damage, Vector3 targetPosition)
        {
            _damage = damage;
            _direction = (targetPosition - transform.position).normalized;
            _updateManager.AddToUpdateList(this);
            _launchTime = Time.time;
        }

        public void DoUpdate()
        {
            if (_launchTime < Time.time - _config.LifeTime)
            {
                DestroyObject();
            }
            else
            {
                Move();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == _enemyLayerId)
            {
                var target = other.GetComponent<Health>();

                if (target != null)
                {
                    target.Damage(_damage);
                    DestroyObject();
                }
            }
        }

        protected void DestroyObject()
        {
            _updateManager.RemoveFromList(this);
            ObjectPool.Recycle(gameObject);
        }
    }
}