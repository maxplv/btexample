﻿using UnityEngine;

namespace BTExample.BT
{
    public class BTInverter : BTNode
    {
        [SerializeField] private BTNode _node;
        
        public override BTNodeStates Evaluate()
        {
            switch (_node.Evaluate())
            {
                case BTNodeStates.FAILURE:
                    _nodeState = BTNodeStates.SUCCESS;
                    return _nodeState;
                case BTNodeStates.SUCCESS:
                    _nodeState = BTNodeStates.FAILURE;
                    return _nodeState;
                case BTNodeStates.RUNNING:
                    _nodeState = BTNodeStates.RUNNING;
                    return _nodeState;
            }
            
            _nodeState = BTNodeStates.SUCCESS;
            return _nodeState;
        } 
    }
}