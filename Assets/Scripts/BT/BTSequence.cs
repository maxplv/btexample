﻿using UnityEngine;

namespace BTExample.BT
{
    public class BTSequence : BTNode
    {
        [SerializeField] private BTNode[] _nodes;
        
        public override BTNodeStates Evaluate()
        {
            bool anyChildRunning = false;
         
            for (var i = 0; i < _nodes.Length; i++)
            {
                switch (_nodes[i].Evaluate())
                {
                    case BTNodeStates.FAILURE:
                        _nodeState = BTNodeStates.FAILURE;
                        return _nodeState;       
                    case BTNodeStates.SUCCESS:
                        continue;
                    case BTNodeStates.RUNNING:
                        anyChildRunning = true;
                        continue;
                } 
            }
            
            _nodeState = anyChildRunning ? BTNodeStates.RUNNING : BTNodeStates.SUCCESS;
            return _nodeState;
        }
    }
}