﻿using UnityEngine;

namespace BTExample.BT
{
    public class BTSelector : BTNode
    {
        [SerializeField] private BTNode[] _nodes;

        public override BTNodeStates Evaluate()
        { 
            for (var i = 0; i < _nodes.Length; i++)
            { 
                switch (_nodes[i].Evaluate())
                { 
                    case BTNodeStates.FAILURE: 
                        continue; 
                    case BTNodeStates.SUCCESS: 
                        _nodeState = BTNodeStates.SUCCESS; 
                        return _nodeState; 
                    case BTNodeStates.RUNNING: 
                        _nodeState = BTNodeStates.RUNNING; 
                        return _nodeState;
                }
            }
            
            _nodeState = BTNodeStates.FAILURE;
            return _nodeState; 
        } 
    }
}