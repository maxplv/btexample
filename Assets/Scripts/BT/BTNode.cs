﻿using UnityEngine;

namespace BTExample.BT
{
    public abstract class BTNode : MonoBehaviour
    {
        protected BTNodeStates _nodeState;
        
        public abstract BTNodeStates Evaluate(); 
        
#if UNITY_EDITOR
        private void Update()
        {
            _nodeState = BTNodeStates.FAILURE;
        }

        private void LateUpdate()
        {
            var name = gameObject.name.Split('-');
            gameObject.name = $"{name[0]}-{_nodeState.ToString()}";
        }
#endif
    }
}