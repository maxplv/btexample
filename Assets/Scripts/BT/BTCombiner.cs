﻿using BTExample.Common;
using BTExample.Managers;
using UnityEngine;

namespace BTExample.BT
{
    public class BTCombiner : MonoBehaviour, IUpdatable
    {
        [SerializeField] private BTNode _rootNode;

        private void Start()
        {
            ManagersFacade.Instance.UpdateManager.AddToUpdateList(this);
        }

        private void OnDestroy()
        {
            ManagersFacade.Instance.UpdateManager.RemoveFromList(this);
        }

        public void DoUpdate()
        {
            _rootNode.Evaluate();
        }
    }
}
