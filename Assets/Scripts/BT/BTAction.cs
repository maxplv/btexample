﻿namespace BTExample.BT
{
    public class BTAction : BTNode
    {
        public delegate BTNodeStates ActionNodeDelegate();
        
        private ActionNodeDelegate _action = () => BTNodeStates.FAILURE;

        public void InjectAction(ActionNodeDelegate action)
        {
            _action = action;
        }
        
        public override BTNodeStates Evaluate()
        {
            _nodeState = _action.Invoke();
            return _nodeState;
        }
    }
}
