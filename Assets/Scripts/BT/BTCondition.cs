﻿using UnityEngine;

namespace BTExample.BT
{
    public class BTCondition : BTNode
    {
        public delegate bool ConditionNodeDelegate();

        [SerializeField] private BTNode _trueNode;
        [SerializeField] private BTNode _falseNode;
        
        private ConditionNodeDelegate _conditionCallback = () => false;

        public void InjectCondition(ConditionNodeDelegate conditionCallback)
        {
            _conditionCallback = conditionCallback;
        }
        
        public override BTNodeStates Evaluate()
        {
            _nodeState = _conditionCallback.Invoke() ? _trueNode.Evaluate() : _falseNode.Evaluate();
            return _nodeState;
        }
    }
}
