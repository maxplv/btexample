﻿namespace BTExample.BT
{
    public enum BTNodeStates
    {
        FAILURE = 0,
        SUCCESS = 1,
        RUNNING = 2,
    }
}