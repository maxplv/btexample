using UnityEngine;

namespace BTExample.Configs
{
    [CreateAssetMenu(fileName = "ShellData", menuName = "Configs/ShellData", order = 0)]
    public class ShellConfig : ScriptableObject
    {
        [SerializeField] private float _speed;
        [SerializeField] private int _lifeTime;
        [SerializeField] private AnimationCurve _trajectory;
        [SerializeField] private float _maxHeight;
        
        public float Speed => _speed;
        public int LifeTime => _lifeTime;
        public AnimationCurve Trajectory => _trajectory;
        public float MaxHeight => _maxHeight;
    }
}