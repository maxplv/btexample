﻿using UnityEngine;

namespace BTExample.Configs
{
    [CreateAssetMenu(fileName = "UnitData", menuName = "Configs/UnitData", order = 0)]
    public class UnitConfig : ScriptableObject
    {
        [SerializeField] private int _hp;
        [SerializeField] private float _speed;
        [SerializeField] private int _basicDamage;
        [SerializeField] private float _hitDelay;
        [SerializeField] private int _runAwayHealth;
        [SerializeField] private float _bonusAttackProbability;

        public int HP => _hp;
        public float Speed => _speed;
        public int BasicDamage => _basicDamage;
        public float HitDelay => _hitDelay;
        public int RunAwayHealth => _runAwayHealth;
        public float BonusAttackProbability => _bonusAttackProbability;
    }
}