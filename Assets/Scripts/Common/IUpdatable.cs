﻿namespace BTExample.Common
{
    public interface IUpdatable
    {
        void DoUpdate();
    }
}