﻿using System.Collections.Generic;
using BTExample.Common;
using UnityEngine;

namespace BTExample.Managers
{
    public class UpdateManager : MonoBehaviour
    {
        private List<IUpdatable> _updatables = new List<IUpdatable>();

        public void AddToUpdateList(IUpdatable updatable)
        {
            if (!_updatables.Contains(updatable))
            {
                _updatables.Add(updatable);
            }
        }

        public void RemoveFromList(IUpdatable updatable)
        {
            _updatables.Remove(updatable);
        }

        private void Update()
        {
            for (int i = 0; i < _updatables.Count; i++)
            {
                _updatables[i].DoUpdate();
            }
        }
    }
}