﻿using UnityEngine;

namespace BTExample.Managers
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] private Joystick _joystick;

        public Joystick Joystick => _joystick;
    }
}