﻿using UnityEngine;

namespace BTExample.Managers
{
    public class ManagersFacade : MonoBehaviour
    {
        [SerializeField] private InputManager _inputManager;
        [SerializeField] private UpdateManager _updateManager;

        private static ManagersFacade _instance;

        public static ManagersFacade Instance => _instance;
        
        public InputManager InputManager => _inputManager;
        public UpdateManager UpdateManager => _updateManager;

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else if(_instance != this)
            {
                Destroy(gameObject);
            }
        }
    }
}